-- ELIMINAR TABLAS ANTES DE INICIAR
DROP TABLE SALE_DETAIL IF EXISTS;
DROP TABLE SALE IF EXISTS;
DROP TABLE PRODUCTO IF EXISTS;
DROP TABLE PRODUCTO_CATEGORIA IF EXISTS;
DROP TABLE CLIENTE IF EXISTS;

-- CREAR TABLAS
CREATE TABLE CLIENTE (cliente_id int NOT NULL AUTO_INCREMENT PRIMARY KEY, 
                      nombre varchar(150) not null, 
                      apellido varchar(150) not null, 
                      cedula int not null, 
                      direccion varchar(200)); 

CREATE TABLE PRODUCTO_CATEGORIA (categoria_id int not null AUTO_INCREMENT PRIMARY KEY, 
                                  nombre varchar(150) not null,
                                  descripcion varchar(150) not null);

CREATE TABLE PRODUCTO (producto_id int not null AUTO_INCREMENT PRIMARY KEY, 
                       nombre varchar(150) not null,
 					   detalle varchar(150) not null,
                       categoria_id int NULL,
                       stock int, precio_unitario float,
                       CONSTRAINT FK_Categoria FOREIGN KEY (categoria_id)
                       REFERENCES PRODUCTO_CATEGORIA (categoria_id)
                       );

CREATE TABLE FACTURA (factura_id int not null AUTO_INCREMENT PRIMARY KEY,
 					cliente_id int NULL,
 					fecha datetime not null,
 					subtotal float not null, 
 					total float not null,
 					CONSTRAINT FK_Cliente FOREIGN KEY (cliente_id)
                    REFERENCES CLIENTE (cliente_id)
 					);

CREATE TABLE FACTURA_DETALLE (detalle_id int not null AUTO_INCREMENT PRIMARY KEY,
 							factura_id int NULL,
 							producto_id int NULL,
 							cantidad int not null,
 							precio float not null,
 							CONSTRAINT FK_Factura FOREIGN KEY (factura_id)
                      	 	REFERENCES FACTURA (factura_id),
 							CONSTRAINT FK_Producto FOREIGN KEY (producto_id)
                      	 	REFERENCES PRODUCTO (producto_id)
 							); 