INSERT INTO CLIENTE (nombre, apellido, cedula, direccion) values ('Julissa', 'Silva', 0926427220, 'Guayaquil'), ('Stef', 'Panchana', 0930124110, 'Guayaquil');

INSERT INTO PRODUCTO_CATEGORIA (nombre, descripcion) values ('Audio','Parlantes'), ('Video','Camaras IP'), ('Computadoras','Portatiles');

INSERT INTO PRODUCTO (nombre, detalle, categoria_id, stock, precio_unitario) values  ('Dell 250H', 'Laptop Comercial',(select categoria_id  from PRODUCTO_CATEGORIA where categoria_id = 3), '5', 750.0);
INSERT INTO PRODUCTO (nombre, detalle, categoria_id, stock, precio_unitario) values  ('RCA 50w', 'Barra de Sonido RCA',(select categoria_id  from PRODUCTO_CATEGORIA where categoria_id = 1), '8', 90.0);

