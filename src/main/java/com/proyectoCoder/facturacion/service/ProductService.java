package com.proyectoCoder.facturacion.service;

import com.proyectoCoder.facturacion.dto.ProductDto;
import com.proyectoCoder.facturacion.model.Product;
import com.proyectoCoder.facturacion.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    public Product create(Product product)
    {
        return this.productRepository.save(product);
    }

    public ProductDto getById(int id) throws Exception
    {
        if (id <= 0) { throw new Exception("Producto Inválido.");	}

        Product product = this.productRepository.findById(id).get();

        ProductDto productoDetail = new ProductDto();
        productoDetail.setNombre(product.getNombre());
        productoDetail.setDetalle(product.getDetalle());
        productoDetail.setCategoria(product.getCategoria().getDescripcion());
        productoDetail.setStock(product.getStock());
        productoDetail.setPrecioUnitario(product.getPrecioUni());

        return productoDetail;
    }

    public Product updateById(Product product, int id) throws Exception
    {
        if (id <= 0) { throw new Exception("Producto Inválido."); }

        Product updatingProduct = this.productRepository.findById(id).get();

        updatingProduct.setNombre(product.getNombre());
        updatingProduct.setDetalle(product.getDetalle());
        updatingProduct.setStock(product.getStock());
        updatingProduct.setPrecioUni(product.getPrecioUni());
        updatingProduct.setCategoria(product.getCategoria());

        return this.productRepository.save(updatingProduct);
    }

    public void deleteById(int id) throws Exception
    {
        if (id <= 0) { throw new Exception("Producto Inválido."); }

        this.productRepository.deleteById(id);
    }

    public List<ProductDto> getAllProducts() throws Exception
    {
        List<ProductDto> all = new ArrayList<>();

        for (int i = 0; i < this.productRepository.findAll().size(); i++)
        {
            ProductDto productoDetail = new ProductDto();
            productoDetail.setNombre(this.productRepository.findAll().get(i).getNombre());
            productoDetail.setDetalle(this.productRepository.findAll().get(i).getDetalle());
            productoDetail.setCategoria(this.productRepository.findAll().get(i).getCategoria().getDescripcion());
            productoDetail.setStock(this.productRepository.findAll().get(i).getStock());
            productoDetail.setPrecioUnitario(this.productRepository.findAll().get(i).getPrecioUni());

            all.add(productoDetail);
        }

        return all;
    }
}
