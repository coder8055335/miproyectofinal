package com.proyectoCoder.facturacion.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class DateTimeRestApi {

    public String getLocalDateTime()
    {
        RestTemplate restTemplate = new RestTemplate();
        final String url = "http://worldclockapi.com/api/json/utc/now";
        return restTemplate.getForObject(url, String.class);
    }
}
