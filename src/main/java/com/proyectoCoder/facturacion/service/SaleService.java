package com.proyectoCoder.facturacion.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.proyectoCoder.facturacion.dto.ProductDto;
import com.proyectoCoder.facturacion.dto.SaleDetailDto;
import com.proyectoCoder.facturacion.model.Client;
import com.proyectoCoder.facturacion.model.Product;
import org.json.JSONArray;
import org.json.JSONObject;
import com.proyectoCoder.facturacion.dto.SaleDto;
import com.proyectoCoder.facturacion.dto.SalePrint;
import com.proyectoCoder.facturacion.model.Sale;
import com.proyectoCoder.facturacion.model.SaleDetail;
import com.proyectoCoder.facturacion.repository.ClientRepository;
import com.proyectoCoder.facturacion.repository.ProductRepository;
import com.proyectoCoder.facturacion.repository.SaleDetailRepository;
import com.proyectoCoder.facturacion.repository.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@Service
public class SaleService {

    @Autowired
    private SaleRepository saleRepository;

    @Autowired
    private SaleDetailRepository saleDetailRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private DateTimeRestApi dateTimeRestApi;

    public Sale create(SaleDto newSale) throws Exception {
        //**** Sección de Variables Locales ****//
        double subtotal = 0.0;//Creo variable para controlar subtotal de los items ingresados
        List<SaleDetail> saleDetailList = new ArrayList<>();//Creo listado de items procesados
        Sale newSaleProcess = new Sale();//Creo registro de nueva factura

        var clienteId = newSale.getCliente().getId();
        var opCliente = this.clientRepository.findById(clienteId);

        //**** Verifico si existe el cliente para facturación ****//
        if (opCliente.isEmpty()) {
            throw new Exception("El cliente con Id: " + clienteId + ", no existe, realice la creación del nuevo cliente para continuar.");
        }
        else
        {
            newSaleProcess.setCliente(opCliente.get());

            //Obtener la fecha y hora actual, usando API externa
            //Verificar que se tenga devolución del servicio Api Rest Externo
            if (!dateTimeRestApi.getLocalDateTime().isEmpty())
            {
                JSONObject obj = new JSONObject(dateTimeRestApi.getLocalDateTime());
                String timeStr = obj.getString("currentDateTime");
                int count = timeStr.length();
                String timeProcess = timeStr.substring(0, count - 1);
                timeProcess = timeProcess + ":00";
                LocalDateTime dateTime = LocalDateTime.parse(timeProcess).minusHours(5);

                //Seteo la fecha y hora usando un api rest externo
                newSaleProcess.setFechaFactura(dateTime.toLocalDate());
            }
            else
                newSaleProcess.setFechaFactura(LocalDate.now());//Obtener la fecha y hora actual, usando LocalDateTime

            //Procesar items del detalle
            //Creo registro de detalle de factura
            if (newSale.getListBaseProducto().size() > 0)
            {
                for (int i = 0; i < newSale.getListBaseProducto().size(); i++ )
                {
                    var productoId = newSale.getListBaseProducto().get(i).getProducto().getId();
                    var opProducto = this.productRepository.findById(productoId);

                    //Comprobar si existe producto
                    if (opProducto.isEmpty()) {
                        throw new Exception("El producto con Id: " + newSale.getListBaseProducto().get(i).getProducto().getId() + " , No Existe, por favor modifique su compra!");
                    }
                    else
                    {
                        SaleDetail newDetail = new SaleDetail();
                        newDetail.setProducto(opProducto.get());

                        //Comprobar si existe stock disponible
                        if (newSale.getListBaseProducto().get(i).getCantidad() <= opProducto.get().getStock())
                        {
                            newDetail.setCantidad(newSale.getListBaseProducto().get(i).getCantidad());

                            //Recuperar informacon y procesar informacion
                            newDetail.setPrecio(opProducto.get().getPrecioUni());

                            subtotal = subtotal + newDetail.getCantidad() * newDetail.getPrecio();

                            //Guardo la informacion del detalle procesado sin novedad
                            saleDetailList.add(newDetail);
                        }
                        else
                            throw new Exception("El stock del producto solicitado es: " + opProducto.get().getStock() + ", Modifique su compra para continuar!");
                    }
                }
            }
            else
            {
                //Mensaje de que advertencia de que no existen registros de items
                throw new Exception("No se puede procesar la compra por falta de items para comprar.");
            }
        }

        //Actualizar el Stock de los productos registrados en la venta
        if (saleDetailList.size() > 0)
        {
            for (int j = 0; j < saleDetailList.size(); j++)
            {
                actualizarStock(saleDetailList.get(j).getProducto(), saleDetailList.get(j).getCantidad());
            }
        }

        //Genero subtotal y total (incluye impuestos ECU)
        newSaleProcess.setSubtotal(subtotal);
        newSaleProcess.setTotal( (subtotal * 0.12) + (subtotal));

        //Guardo la cabecera de la factura y recupero el ID
        newSaleProcess = this.saleRepository.save(newSaleProcess);

        for (SaleDetail item : saleDetailList)
        {
            //Seteo el Id de la cabecera en el detalle de la factura
            item.setFactura(newSaleProcess);

            //Guardo detalle de la factura
            this.saleDetailRepository.save(item);
        }

        return newSaleProcess;
    }

    public SalePrint getById(int id) throws Exception
    {
        if (id <= 0)
        {
            throw new Exception("Factura no encontrada");
        }

        SalePrint newSale = null;
        var saleId = this.saleRepository.findById(id);

        //Comprobar si existe producto
        if (saleId.isEmpty()) {
            throw new Exception("La factura de Id: " + id + " , No Existe, por favor realice una facturación previamente antes de consultar!");
        }
        else
        {
            newSale = new SalePrint();
            List<SaleDetailDto> listSaleDetail = new ArrayList<>();

            var saleHeader = this.saleRepository.getReferenceById(id);
            var saleDetail = this.saleDetailRepository.getAllDetail(id);

            newSale.setSaleId(id);
            newSale.setCliente(saleHeader.getCliente());
            newSale.setFecha(saleHeader.getFechaFactura());
            newSale.setSubtotal(saleHeader.getSubtotal());
            newSale.setTotal(saleHeader.getTotal());

            for (int i = 0; i < saleDetail.size(); i++)
            {
                SaleDetailDto newDetail = new SaleDetailDto();
                newDetail.producto = saleDetail.get(i).getProducto().getNombre();
                newDetail.cantidad = saleDetail.get(i).getCantidad();
                newDetail.precio = saleDetail.get(i).getPrecio();

                listSaleDetail.add(newDetail);
            }


            newSale.setLineas(listSaleDetail);
        }

        return newSale;
    }

    private void actualizarStock(Product producto, int cantidadVendida) {

        var productoDB = this.productRepository.getById(producto.getId());
        var stock = productoDB.getStock();
        var nuevoStock = stock - cantidadVendida;
        productoDB.setStock(nuevoStock);

        this.productRepository.save(productoDB);
    }

    public List<SalePrint> getAllSales() throws Exception
    {
        List<SalePrint> all = new ArrayList<>();
        var salePrintAll = this.saleRepository.findAll();

        for (int i = 0; i < salePrintAll.size(); i++)
        {
            List<SaleDetailDto> listSaleDetail = new ArrayList<>();
            SalePrint newSale = new SalePrint();

            newSale.setSaleId(i);
            newSale.setCliente(salePrintAll.get(i).getCliente());
            newSale.setFecha(salePrintAll.get(i).getFechaFactura());
            newSale.setSubtotal(salePrintAll.get(i).getSubtotal());
            newSale.setTotal(salePrintAll.get(i).getTotal());

            var saleDetail = this.saleDetailRepository.getAllDetail(i);

            for (int j = 0; j < saleDetail.size(); j++)
            {
                SaleDetailDto newDetail = new SaleDetailDto();
                newDetail.producto = saleDetail.get(j).getProducto().getNombre();
                newDetail.cantidad = saleDetail.get(j).getCantidad();
                newDetail.precio = saleDetail.get(j).getPrecio();

                listSaleDetail.add(newDetail);
            }

            newSale.setLineas(listSaleDetail);

            all.add(newSale);
        }

        return all;
    }
}
