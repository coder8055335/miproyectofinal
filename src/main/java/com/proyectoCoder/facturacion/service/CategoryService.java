package com.proyectoCoder.facturacion.service;

import com.proyectoCoder.facturacion.dto.CategoryDto;
import com.proyectoCoder.facturacion.model.ProductCategory;
import com.proyectoCoder.facturacion.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    public ProductCategory create(ProductCategory newCategory) throws Exception
    {
        return this.categoryRepository.save(newCategory);
    }

    public CategoryDto getById(int id) throws Exception
    {
        if (id <= 0) { throw new Exception("Categoria Inválida.");	}

        ProductCategory productCategory = this.categoryRepository.findById(id).get();
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setNombre(productCategory.getNombre());
        categoryDto.setDescripcion(productCategory.getDescripcion());
        categoryDto.setId(productCategory.getId());

        return categoryDto;
    }

    public ProductCategory findById(int id) throws Exception
    {
        if (id <= 0) { throw new Exception("Categoria Inválida.");	}

        return this.categoryRepository.findById(id).get();
    }

    public ProductCategory updateById(ProductCategory productCategory, int id) throws Exception
    {
        if (id <= 0) { throw new Exception("Categoria Inválida."); }

        ProductCategory updatingCategory = this.categoryRepository.findById(id).get();

        updatingCategory.setNombre(productCategory.getNombre());
        updatingCategory.setDescripcion(productCategory.getDescripcion());

        return this.categoryRepository.save(updatingCategory);
    }

    public void deleteById(int Id) throws Exception
    {
        if (Id <= 0) { throw new Exception("Categoria Inválida."); }

        this.categoryRepository.deleteById(Id);
    }
}
