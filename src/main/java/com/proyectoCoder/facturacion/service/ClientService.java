package com.proyectoCoder.facturacion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyectoCoder.facturacion.model.Client;
import com.proyectoCoder.facturacion.repository.ClientRepository;

import java.util.List;

@Service
public class ClientService {

	@Autowired
	private ClientRepository clientRepository;
	
	public Client create(Client newClient)
	{
		return this.clientRepository.save(newClient);
	}
	
	public Client getById(int id) throws Exception
	{
		if (id <= 0) { throw new Exception("Cliente Inválido.");	}
		
		return this.clientRepository.findById(id).get();
	}
	
	public Client updateById(Client updateClient, int id) throws Exception
	{
		if (id <= 0) { throw new Exception("Cliente Inválido."); }
		
		Client updatingClient = this.clientRepository.findById(id).get();
		
		updatingClient.setNombre(updateClient.getNombre());
		updatingClient.setApellido(updateClient.getApellido());
		updatingClient.setCedula(updateClient.getCedula());
		updatingClient.setDireccion(updateClient.getDireccion());
		
		return this.clientRepository.save(updatingClient);
	}
	
	public void deleteById(int id) throws Exception
	{
		if (id <= 0) { throw new Exception("Cliente Inválido."); }
		
		this.clientRepository.deleteById(id);
	}

	public List<Client> getAllClient() throws Exception
	{
		List<Client> all = this.clientRepository.findAll();
		return all;
	}

}
