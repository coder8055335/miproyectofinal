package com.proyectoCoder.facturacion.repository;

import com.proyectoCoder.facturacion.dto.SaleDetailDto;
import com.proyectoCoder.facturacion.model.SaleDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaleDetailRepository extends JpaRepository<SaleDetail, Integer> {
    @Query(nativeQuery = true, value = "SELECT t FROM FACTURA_DETALLE t WHERE t.factura_id = ?1")
    SaleDetail getSaleDetailBySaleID(int saleID);

    @Query(nativeQuery = true, value = "SELECT * FROM FACTURA_DETALLE WHERE factura_id = ?1")
    List<SaleDetail> getAllDetail(int saleId);
}
