package com.proyectoCoder.facturacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyectoCoder.facturacion.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer>{

}
