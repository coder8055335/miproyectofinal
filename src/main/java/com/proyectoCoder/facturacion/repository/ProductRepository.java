package com.proyectoCoder.facturacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyectoCoder.facturacion.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>{

}
