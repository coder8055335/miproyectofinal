package com.proyectoCoder.facturacion.controller;

import com.proyectoCoder.facturacion.dto.CategoryDto;
import com.proyectoCoder.facturacion.model.ProductCategory;
import com.proyectoCoder.facturacion.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/category")
public class CategoryController {

    @Autowired
    public CategoryService categoryService;

    @PostMapping(path = "/")
    public Object create(@RequestBody ProductCategory productCategory)
    {
        try
        {
            this.categoryService.create(productCategory);
            return HttpStatus.CREATED;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<CategoryDto> findById(@PathVariable int id) throws Exception
    {
        return new ResponseEntity<>(this.categoryService.getById(id), HttpStatus.OK);
    }
}
