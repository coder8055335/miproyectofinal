package com.proyectoCoder.facturacion.controller;

import com.proyectoCoder.facturacion.dto.ProductDto;
import com.proyectoCoder.facturacion.model.Client;
import com.proyectoCoder.facturacion.model.Product;
import com.proyectoCoder.facturacion.model.ProductCategory;
import com.proyectoCoder.facturacion.service.CategoryService;
import com.proyectoCoder.facturacion.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/producto")
public class ProductController {
    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;

    @PostMapping(path = "/")
    public Object create(@RequestBody ProductDto product)
    {
        try {
            Product newProduct = new Product();
            newProduct.setNombre(product.getNombre());
            newProduct.setDetalle(product.getDetalle());
            newProduct.setStock(product.getStock());
            newProduct.setPrecioUni(product.getPrecioUnitario());
            newProduct.setCategoria(this.categoryService.findById(Integer.parseInt(product.getCategoria())));

            this.productService.create(newProduct);
            System.out.println("Nuevo registro creado : " + product.toString());
            return HttpStatus.CREATED;
        } catch (Exception e) {
            e.printStackTrace();
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<ProductDto> findById(@PathVariable int id) throws Exception
    {
        return new ResponseEntity<>(this.productService.getById(id), HttpStatus.OK);
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<ProductDto>> findAll() throws Exception
    {
        return new ResponseEntity<>(this.productService.getAllProducts(), HttpStatus.OK);
    }

    @PutMapping(path = "/{id}")
    public Object updateProduct(@RequestBody ProductDto product, @PathVariable int id) throws Exception
    {
        try {
            Product newProduct = new Product();
            newProduct.setNombre(product.getNombre());
            newProduct.setDetalle(product.getDetalle());
            newProduct.setStock(product.getStock());
            newProduct.setPrecioUni(product.getPrecioUnitario());
            newProduct.setCategoria(this.categoryService.findById(Integer.parseInt(product.getCategoria())));

            this.productService.updateById(newProduct, id);
            System.out.println("Se actualizo producto con exito ! ");
            return HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
