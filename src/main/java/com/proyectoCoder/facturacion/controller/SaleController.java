package com.proyectoCoder.facturacion.controller;

import com.proyectoCoder.facturacion.dto.ProductDto;
import com.proyectoCoder.facturacion.dto.SaleDto;
import com.proyectoCoder.facturacion.dto.SalePrint;
import com.proyectoCoder.facturacion.service.SaleService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/sale")
public class SaleController {
    @Autowired
    private SaleService saleService;

    @PostMapping(path = "/")
    public Object create(@RequestBody SaleDto sale)
    {
        Object response = null;
        
        try
        {
            this.saleService.create(sale);
            System.out.println("Nuevo registro creado : " + sale.toString());
            response = HttpStatus.CREATED;
        }
        catch (Exception e) {
            e.printStackTrace();
            response = new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<SalePrint> findById(@PathVariable int id) throws Exception
    {
        return new ResponseEntity<>(this.saleService.getById(id), HttpStatus.OK);
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<SalePrint>> findAll() throws Exception
    {
        return new ResponseEntity<>(this.saleService.getAllSales(), HttpStatus.OK);
    }
}
