package com.proyectoCoder.facturacion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proyectoCoder.facturacion.model.Client;
import com.proyectoCoder.facturacion.service.ClientService;

import java.util.List;

@RestController
@RequestMapping(path = "/cliente")
public class ClientController {

	@Autowired
	public ClientService clientService;
	
	@PostMapping(path = "/")
	public Object create(@RequestBody Client client)
	{
		try
		{
			this.clientService.create(client);
			return HttpStatus.CREATED;
		}
		catch (Exception e) {
			e.printStackTrace();
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
	}
	
	@GetMapping(path = "/{id}")
	public ResponseEntity<Client> findById(@PathVariable int id) throws Exception
	{
		return new ResponseEntity<>(this.clientService.getById(id), HttpStatus.OK);
	}

	@GetMapping(path = "/all")
	public ResponseEntity<List<Client>> findAll() throws Exception
	{
		return new ResponseEntity<>(this.clientService.getAllClient(), HttpStatus.OK);
	}

}
