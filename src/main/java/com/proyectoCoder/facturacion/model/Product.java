package com.proyectoCoder.facturacion.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "PRODUCTO")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PRODUCTO_ID")
	@JsonProperty("producto_id")
	private int id;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "DETALLE")
	private String detalle;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "CATEGORIA_ID")
	private ProductCategory categoria;
	
	@Column(name = "STOCK")
	private int stock;
	
	@Column(name = "PRECIO_UNITARIO")
	private float precioUni;
	
	public Product(){}
	
	public Product(String nombre, String detalle, int stock, float precioUni)
	{
		this.nombre = nombre;
		this.detalle = detalle;
		this.stock = stock;
		this.precioUni = precioUni;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	
	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public ProductCategory getCategoria() {
		return categoria;
	}

	public void setCategoria(ProductCategory categoria) {
		this.categoria = categoria;
	}

	public float getPrecioUni() {
		return precioUni;
	}

	public void setPrecioUni(float precioUni) {
		this.precioUni = precioUni;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", nombre=" + nombre + ", detalle=" + detalle + ", categoria=" + categoria
				+ ", stock=" + stock + ", precioUni=" + precioUni + "]";
	}
}
