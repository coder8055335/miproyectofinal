package com.proyectoCoder.facturacion.model;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "FACTURA_DETALLE")
public class SaleDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DETALLE_ID")
	private int id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "FACTURA_ID")
	private Sale factura;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCTO_ID")
	private Product producto;
	
	@Column(name = "CANTIDAD")
	private int cantidad;
	
	@Column(name = "PRECIO")
	private double precio;
	
	
	public SaleDetail() {}


	public Sale getFactura() {
		return factura;
	}


	public void setFactura(Sale factura) {
		this.factura = factura;
	}


	public Product getProducto() {
		return producto;
	}


	public void setProducto(Product producto) {
		this.producto = producto;
	}


	public int getCantidad() {
		return cantidad;
	}


	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	
}
