package com.proyectoCoder.facturacion.dto;

import com.proyectoCoder.facturacion.model.Product;

public class BaseProductDto {

    private int cantidad;

    private Product producto;

    public BaseProductDto(int cantidad, Product producto) {
        this.cantidad = cantidad;
        this.producto = producto;
    }

    public BaseProductDto() {
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Product getProducto() {
        return producto;
    }

    public void setProducto(Product producto) {
        this.producto = producto;
    }

    @Override
    public String toString() {
        return "BaseProductDto{" +
                "cantidad=" + cantidad +
                ", producto=" + producto +
                '}';
    }
}
