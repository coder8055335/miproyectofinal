package com.proyectoCoder.facturacion.dto;

public class ProductDto {

    private String nombre;

    private String detalle;

    private String categoria;

    private int stock;

    private float precioUnitario;

    public ProductDto() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "nombre='" + nombre + '\'' +
                ", detalle='" + detalle + '\'' +
                ", categoria='" + categoria + '\'' +
                ", stock=" + stock +
                ", precioUnitario=" + precioUnitario +
                '}';
    }
}
