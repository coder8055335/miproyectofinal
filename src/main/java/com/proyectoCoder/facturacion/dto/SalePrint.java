package com.proyectoCoder.facturacion.dto;

import com.proyectoCoder.facturacion.model.Client;
import com.proyectoCoder.facturacion.model.SaleDetail;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public class SalePrint {

    private Integer saleId;

    private LocalDate fecha;

    private double subtotal;
    private double total;

    private Client cliente;

    private List<SaleDetailDto> lineas;


    public SalePrint() {
    }

    public Integer getSaleId() {
        return saleId;
    }

    public void setSaleId(Integer saleId) {
        this.saleId = saleId;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Client getCliente() {
        return cliente;
    }

    public void setCliente(Client cliente) {
        this.cliente = cliente;
    }

    public List<SaleDetailDto> getLineas() {
        return lineas;
    }

    public void setLineas(List<SaleDetailDto> lineas) {
        this.lineas = lineas;
    }

    @Override
    public String toString() {
        return "SalePrint{" +
                "saleId=" + saleId +
                ", fecha=" + fecha +
                ", subtotal=" + subtotal +
                ", total=" + total +
                ", cliente=" + cliente +
                ", lineas=" + lineas +
                '}';
    }
}
