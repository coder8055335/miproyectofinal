package com.proyectoCoder.facturacion.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.proyectoCoder.facturacion.model.Client;
import com.proyectoCoder.facturacion.model.SaleDetail;

import java.time.LocalDate;
import java.util.List;

public class SaleDto {

    private Client cliente;

    @JsonProperty("lista")
    private List<BaseProductDto> listBaseProducto;

    public SaleDto() {
    }

    public SaleDto(Client cliente, List<BaseProductDto> listBaseProducto) {
        this.cliente = cliente;
        this.listBaseProducto = listBaseProducto;
    }

    public Client getCliente() {
        return cliente;
    }

    public void setCliente(Client cliente) {
        this.cliente = cliente;
    }

    public List<BaseProductDto> getListBaseProducto() {
        return listBaseProducto;
    }

    public void setListBaseProducto(List<BaseProductDto> listBaseProducto) {
        this.listBaseProducto = listBaseProducto;
    }

    @Override
    public String toString() {
        return "SaleDto{" +
                "cliente=" + cliente +
                ", listBaseProducto=" + listBaseProducto +
                '}';
    }
}
