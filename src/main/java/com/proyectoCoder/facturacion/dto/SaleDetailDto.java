package com.proyectoCoder.facturacion.dto;

import com.proyectoCoder.facturacion.model.Product;
import com.proyectoCoder.facturacion.model.Sale;
import jakarta.persistence.*;

public class SaleDetailDto {

    public String producto;

    public int cantidad;

    public double precio;
}
