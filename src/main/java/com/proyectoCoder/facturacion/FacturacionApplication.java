package com.proyectoCoder.facturacion;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FacturacionApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(FacturacionApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		try
		{
	        System.out.println("********************************************************************");
	        System.out.println("This is my Second Presentation of End Proyect - GPA: 07/08/2023 ");
	        System.out.println("********************************************************************");
	        
			//##########################################################################
			//La presente clase tiene el objetivo de poder verificar la funcionalidad
			//de las capas Servicio, Controlador, Repositorio, Clases de Visualización de datos o Clases de Entrada de Datos compuestos
			//para Cliente, Categoria de Producto, Producto y registro de una Factura con su detalle
			//del segundo entregable.
			//##########################################################################
			
	        System.out.println("*************************  Aplicación Iniciada   ****************************");
		}
		catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
	}
	
}
